package fr.dauphine.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.List;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Service;

public class Logger {
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	final static String FILE_NAME_SINGLE = "single.log";
	final static String FILE_NAME_COMPOSITE = "composite.log";
	
	List<ServiceData> serviceData;
	Graph<Service, Number> graph;
	double totalExecutionTime;
	
	public Logger(List<ServiceData> serviceData, Graph<Service, Number> graph,
			double totalExecutionTime) {
		super();
		this.serviceData = serviceData;
		this.graph = graph;
		this.totalExecutionTime = totalExecutionTime;
	}
	

	
	public void writeSingleServices() {
		
		PrintWriter writer;
		try { 
			writer = new PrintWriter(new FileWriter(FILE_NAME_SINGLE, true));
			
			for(Service s:graph.getVertices())
				writer.write(getRecord(s));
			
			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
	
	public void writeSubgraphs() {
		
		setExecutionTime();
		
		 List<Graph<Service, Number>> subgraphs = GraphUtils.getAllSubgraphs(graph);
		 
		 PrintWriter writer;
			try { 
				writer = new PrintWriter(new FileWriter(FILE_NAME_COMPOSITE, true));
				
				for(Graph<Service, Number> g:subgraphs) {
					writer.write(g.toString());
					writer.write(getGraphInfo(g));
				}
				
				writer.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	}



	private void setExecutionTime() {
		for(Service s:graph.getVertices())
			 s.setEstimatedExecutionTime(getServiceData(s).getExecutionTime());
	}



	private String getGraphInfo(Graph<Service, Number> g) {
		String graphInfo = "\n";
		GraphUtils.addControlNodes(g);
		GraphAnalyser analyser = new GraphAnalyser(g);
		double executionTime = 
				analyser.getEstimatedExecutionTime(Constants.INITIAL_NODE, Constants.FINAL_NODE);
		graphInfo += (new Double(executionTime));
		
		graphInfo += " " + isSuccessful(g);
		
		graphInfo += " " + getTimeStamp(g);
		
		graphInfo += "\n";
		
		return graphInfo;
	}



	private Timestamp getTimeStamp(Graph<Service, Number> g) {
		Timestamp timestamp = new Timestamp(Long.MAX_VALUE);
		
		for(Service s:graph.getVertices())
			if(getServiceData(s).getTimeStamp().compareTo(timestamp) < 0)
				timestamp = getServiceData(s).getTimeStamp();
		
		return timestamp;
	}



	private boolean isSuccessful(Graph<Service, Number> g) {
		boolean success = true;
		
		for(Service s:graph.getVertices())
			if(!getServiceData(s).isSuccess())
					return false;
		return success;
	}



	private String getRecord(Service s) {
		ServiceData data = getServiceData(s);
		return s.getName() + " " + data.executionTime + " " + data.success + " " + data.timeStamp + "\n";
	}
	
	private ServiceData getServiceData(Service s) {
		return serviceData.get(serviceData.indexOf(new ServiceData(s.getName())));
	}
	
	
}
