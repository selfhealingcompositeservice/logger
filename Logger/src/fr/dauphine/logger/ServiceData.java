package fr.dauphine.logger;

import java.io.Serializable;
import java.sql.Timestamp;

public class ServiceData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name;
	double executionTime;
	double passedTime;
	boolean success;
	Timestamp timeStamp;
	double finalCost;
	double finalReputation;
	private int failuresNumber;
	
	public ServiceData(String name) {
		this.name = name;
	}
	
	public String getServiceName() {
		return name;
	}

	public double getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(double executionTime) {
		this.executionTime = executionTime;
	}

	public double getPassedTime() {
		return passedTime;
	}

	public void setPassedTime(double passedTime) {
		this.passedTime = passedTime;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Timestamp getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public double getFinalCost() {
		return finalCost;
	}

	public void setFinalCost(double finalCost) {
		this.finalCost = finalCost;
	}

	public double getFinalReputation() {
		return finalReputation;
	}

	public void setFinalReputation(double finalReputation) {
		this.finalReputation = finalReputation;
	}

	@Override
	public String toString() {
		return "ServiceData [name=" + name + ", executionTime=" + executionTime
				+ ", passedTime=" + passedTime + ", success=" + success
				+ ", timeStamp=" + timeStamp + "]";
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceData other = (ServiceData) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void setFailuresNumber(int failuresNumber) {
		this.failuresNumber = failuresNumber;
		
	}

	public int getFailuresNumber() {
		return failuresNumber;
	}
	
}
